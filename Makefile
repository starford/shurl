PKGS := $(shell go list ./... | grep -v /vendor)
ROOTDIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
IMGV ?= latest
test:
		go test -race -cover $(PKGS)

image-%:
		docker build -t registry.gitlab.com/starford/shurl/$*:$(IMGV) $(ROOTDIR)var/$*/
		docker push registry.gitlab.com/starford/shurl/$*:$(IMGV)

dashboard-build:
		go build -race -o $(ROOTDIR)var/dashboard/bin/dashboard $(ROOTDIR)cmd/dashboard/main.go
		cp $(ROOTDIR)page/index.html $(ROOTDIR)var/dashboard/shurl/
		$(MAKE) image-dashboard

build:
		$(MAKE) dashboard-build

up:
		docker-compose up -d

pull:
		docker-compose pull

stop:
		docker-compose stop

down:
		docker-compose down -v
