package shorturl

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

//	RedirectHandler() - redirects to saved long url in cache by id
func (s *ShortUrl) RedirectHandler(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/"):]
	longURL, ok := s.Cache.Get(id)
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		if _, err := w.Write([]byte("short url does not exists")); err != nil {
			logrus.WithFields(
				logrus.Fields{
					"location": `in shurl.*ShortUrl_RedirectHandler() on w.Write()`,
				},
			).Error(err)
		}
		return
	}
	http.Redirect(w, r, longURL.(string), http.StatusFound)
}
