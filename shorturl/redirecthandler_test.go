package shorturl

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/hashicorp/golang-lru/simplelru"
)

func TestShortUrl_RedirectHandler(t *testing.T) {
	lru, err := simplelru.NewLRU(1, nil)
	if err != nil {
		t.Fatal(err)
	}
	domain := "http://localhost:3000"
	sh := ShortUrl{
		Cache:  lru,
		Domain: domain,
	}
	id := "abc"
	sh.Cache.Add(id, "https://google.com")

	req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s/%s", domain, id), nil)
	w := httptest.NewRecorder()
	sh.RedirectHandler(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	expectedStatus := http.StatusFound
	if resp.StatusCode != expectedStatus {
		t.Fatalf(`expected to get 302 status, but got: %v`, resp.StatusCode)
	}
	expectedBody := `<a href="https://google.com">Found</a>.`
	if !strings.Contains(string(body), expectedBody) {
		t.Fatalf("expected to get: %s but got: %s", string(body), expectedBody)
	}
}

func TestShortUrl_RedirectHandler_NoValueInCache(t *testing.T) {
	lru, err := simplelru.NewLRU(1, nil)
	if err != nil {
		t.Fatal(err)
	}
	domain := "http://localhost:3000"
	sh := ShortUrl{
		Cache:  lru,
		Domain: domain,
	}
	id := "abc"

	req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("%s/%s", domain, id), nil)
	w := httptest.NewRecorder()
	sh.RedirectHandler(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	expectedStatus := http.StatusNotFound
	if resp.StatusCode != expectedStatus {
		t.Fatalf(`expected to get "%v" status, but got: "%v"`, expectedStatus, resp.StatusCode)
	}

	expectedErrBody := "short url does not exists"
	if !strings.Contains(string(body), expectedErrBody) {
		t.Fatalf(`expected to get: "%s" but got: "%s"`, string(body), expectedErrBody)
	}
}
