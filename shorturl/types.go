package shorturl

import "github.com/hashicorp/golang-lru/simplelru"

type ShortUrl struct {
	Cache  *simplelru.LRU
	Domain string
}
