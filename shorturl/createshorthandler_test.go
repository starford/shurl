package shorturl

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/hashicorp/golang-lru/simplelru"
)

func TestShortUrl_CreateShortUrlHandler(t *testing.T) {
	lru, err := simplelru.NewLRU(1, nil)
	if err != nil {
		t.Fatal(err)
	}
	domain := "http://localhost:3000"
	sh := ShortUrl{
		Cache:  lru,
		Domain: domain,
	}

	form := "longUrl=https://google.com"

	req := httptest.NewRequest(http.MethodPost, domain, bytes.NewBuffer([]byte(form)))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	sh.CreateShortUrlHandler(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	expectedStatus := http.StatusOK
	if resp.StatusCode != expectedStatus {
		t.Fatalf(`expected to get %v status, but got: %v`, expectedStatus, resp.StatusCode)
	}

	shurl := string(body)[len(fmt.Sprintf("%s/", domain)):]

	expected := "https://google.com"
	if val, ok := sh.Cache.Get(shurl); !ok {
		t.Fatalf("expected to get value from cache: %s, but got: %s", expected, val)
	}
}

func TestShortUrl_CreateShortUrlHandler_EmptyFormValue(t *testing.T) {
	lru, err := simplelru.NewLRU(1, nil)
	if err != nil {
		t.Fatal(err)
	}
	domain := "http://localhost:3000"
	sh := ShortUrl{
		Cache:  lru,
		Domain: domain,
	}

	req := httptest.NewRequest(http.MethodPost, domain, nil)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	sh.CreateShortUrlHandler(w, req)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	expectedStatus := http.StatusBadRequest
	if resp.StatusCode != expectedStatus {
		t.Fatalf(`expected to get "%v" status, but got: "%v"`, expectedStatus, resp.StatusCode)
	}

	expectedErrBody := "longUrl param is empty."
	if !strings.Contains(string(body), expectedErrBody) {
		t.Fatalf(`expected to get: "%s" but got: "%s"`, string(body), expectedErrBody)
	}
}
