package shorturl

import (
	"fmt"
	"net/http"

	"gitlab.com/starford/shurl/utils"

	"github.com/sirupsen/logrus"
)

//	CreateShortUrlHandler() - creates id for long url and write it to cache
func (s *ShortUrl) CreateShortUrlHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		logrus.WithFields(
			logrus.Fields{
				"location": `in handlers.CreateShortUrl() on r.ParseForm()`,
			},
		).Error(err)
	}
	longUrl := r.Form.Get("longUrl")
	if longUrl == "" {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte("longUrl param is empty.")); err != nil {
			logrus.WithFields(
				logrus.Fields{
					"location": `in shurl.*ShortUrl_CreateShortUrlHandler() on w.Write()`,
				},
			).Error(err)
		}
	}

	id := utils.RandStringBytes(6)

	s.Cache.Add(id, longUrl)

	w.WriteHeader(http.StatusOK)
	if _, err = w.Write([]byte(fmt.Sprintf("%s/%s", s.Domain, id))); err != nil {
		logrus.WithFields(
			logrus.Fields{
				"location": `in shurl.*ShortUrl_CreateShortUrlHandler() on w.Write()`,
			},
		).Error(err)
	}
}
