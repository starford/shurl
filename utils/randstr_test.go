package utils

import (
	"testing"
)

func TestRandStringBytes(t *testing.T) {
	var l = []int{1, 2, 5, 10, 8}
	var randStrs []string
	for _, v := range l {
		randStrs = append(randStrs, RandStringBytes(v))
	}

	var expectedLen = []int{1, 2, 5, 10, 8}
	for k, v := range expectedLen {
		lns := len(randStrs[k])
		if lns != v {
			t.Fatalf(`in randstr.TestResolver_GetNoteBook() on conditional "lns != v" - expected len: %v, but got: %v`, v, lns)
		}
	}

}
