package main

import (
	"net/http"

	"gitlab.com/starford/shurl/handlers"

	"github.com/hashicorp/golang-lru/simplelru"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/starford/shurl/shorturl"
)

func main() {
	lru, err := simplelru.NewLRU(200, nil)
	if err != nil {
		logrus.WithFields(
			logrus.Fields{
				"location": `in main() on simplelru.NewLRU()`,
			},
		).Fatal(err)
	}
	sh := shorturl.ShortUrl{
		Cache:  lru,
		Domain: "http://localhost:3000",
	}

	r := chi.NewRouter()
	r.Use(handlers.DisableCORS)

	publicDir := "/var/www/shurl"
	fs := http.FileServer(http.Dir(publicDir))

	r.Method(http.MethodGet, "/", fs)

	r.Get("/{shortUrl}", sh.RedirectHandler)
	r.Post("/create", sh.CreateShortUrlHandler)

	logrus.Fatal(http.ListenAndServe(":3000", r))
}
