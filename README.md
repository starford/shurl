<h1>
    Simple URL Shortener app written on Go
</h1>

>>>
App could be executed in Docker containers, just run:

`$ make up`

Open in your browser:

http://localhost:3000
>>>

<div align="center">
	<img height="500" src="images/shurl.gif" alt="Shurl App">
</div>


